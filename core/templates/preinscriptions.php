<html>
<head>
<meta charset="UTF-8">
<script src="/themes/framadrive/core/nav/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/themes/framadrive/core/nav/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/themes/framadrive/core/nav/lib/bootstrap/css/bootstrap.min.css">
<script src="https://nav6.framasoft.org/nav/nav.js" type="text/javascript"></script>
</head>
<body style="margin:2% 30%">
<?php if (!isset($_POST["InputEmail"])): ?>
<div>
<h1>Framadrive</h1>
<p>Le nombre maximum de comptes Framadrive disponible a été atteint. Les inscriptions sont closes pour le moment.</p>
<p>Elles seront réouvertes si nos ressources le permettent.</p>
<p>En attendant, vous pouvez nous laisser votre adresse email afin que nous vous recontactions lorsque de nouvelles places seront disponibles.</p>
<p>Notez qu'il ne s'agit pas d'une liste d'attente, mais de la possibilité d'être recontacté lorsque Framasoft proposera de nouveaux comptes.</p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
  <div class="form-group">
    <label for="InputEmail">Votre email</label>
    <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Email">
  </div>
 <button type="submit" class="btn btn-default">Soumettre</button>
</form>
</div>
</body>
</html>
<?php else: ?>
<?php
date_default_timezone_set('Europe/Paris');
$file="preinsc.txt";
$email=filter_input(INPUT_POST, 'InputEmail', FILTER_VALIDATE_EMAIL);
$content=date('Y/m/d H:i:s')." - ".$email."\n";
file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
?>
<div>
<h1>Merci !</h1>
<p>Nous vous préviendrons lorsque nous proposerons de nouveau des comptes.</p>
<p>Retour à <a href="https://framadrive.org" title="Retourner sur le site Framadrive.org">Framadrive.org</a></p>
</div>
</body>
</html>
<?php endif ?>